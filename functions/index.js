const functions = require("firebase-functions");
const admin = require("firebase-admin");
const faceapi = require("face-api.js");
const canvas = require("canvas");
var request = require('request');
var dayjs = require('dayjs');
// require("@tensorflow/tfjs");

const { Canvas, Image, ImageData } = canvas;
faceapi.env.monkeyPatch({ Canvas, Image, ImageData });

admin.initializeApp();

exports.scheduledFunction = functions.pubsub.schedule('every 1 hours').onRun(async () => {
  const MODEL_URL = 'weights';
  await faceapi.nets.faceRecognitionNet.loadFromDisk(MODEL_URL);

  let functionLastRunOn = await admin.firestore().collection('summary').doc('functionLastRunAt').get();
  const prevFunctionTime = admin.firestore.Timestamp.fromDate((functionLastRunOn.data()['timeLastRun']).toDate());
  await admin.firestore().collection('summary').doc('functionLastRunAt').update({'timeLastRun': admin.firestore.Timestamp.now()});


  /**
   * Logic of matching:
   *  P - Pictures by Parents
   *  V - Pictures by Volunteers
   *  new - Pictures uploaded after function last ran
   *  old - Pictures uploaded before function last ran
   * if new P > 0, match each new P with all V (where V.createdAt is after P.missingSince)
   * This covers new P with relevant V - both new and old
   * 
   * if new V > 0, match each new V with all old P
   * This along with the above function covers new V with all P
   * 
   * In short - new P & new V matched, new P & old V matched, old P and new V matched
   */

  // getting new V
  let recentChildrenFromVolunteers = await admin.firestore()
    .collectionGroup('childrenAsVolunteer')
    .where('createdAt','>', prevFunctionTime)
    .get();

  // getting new P
  let recentChildrenFromParents = await admin.firestore()
    .collectionGroup('childrenAsParent')
    .where('found','==',false)
    .where('isActive','==',true)
    .where('createdAt','>', prevFunctionTime)
    .get();

  // if new P > 0 function block
  if(recentChildrenFromParents != null && !recentChildrenFromParents.empty) {
    // below code to optimize pictures imported from volunteer side. If there are 2 new P pics, get all V pics after the earliest date of P.missingSince
    const allMissingDates = [];
    for(let childData of recentChildrenFromParents.docs) {
      allMissingDates.push((admin.firestore.Timestamp.fromDate((childData.data()['missingSince']).toDate())));
    }
  
    let earliestUnixDate = Math.min(...allMissingDates);
    console.log(`earlyDate ${earliestUnixDate}`);      
    
    // getting relevant V pics
    let childrenFromVolunteersToBeCompared = await admin.firestore()
      .collectionGroup('childrenAsVolunteer')
      .where('createdAt','>', earliestUnixDate)
      .get();

    // matching
    for(let childDataFromParent of recentChildrenFromParents.docs) {
      for (let childDataFromVolunteer of childrenFromVolunteersToBeCompared.docs) {
        if(childDataFromVolunteer.data()['createdAt'] > childDataFromParent.data()['missingSince']) { // only matching relevant pics
          for(let i = 0; i < childDataFromVolunteer.data()['faceImages'].length; i++) {
            for(let j = 0; j < childDataFromParent.data()['faceImages'].length; j++) {
              let matchFound = await matchFaces(
                childDataFromVolunteer.data()['faceImages'][i],
                childDataFromParent.data()['faceImages'][j],
              );
              if(matchFound) {
                await functionOnMatchFound(childDataFromParent,childDataFromVolunteer,i);
                // exiting loop
                i = childDataFromVolunteer.data()['faceImages'].length;
                j = childDataFromParent.data()['faceImages'].length;
              }
            }
          }
        }
      }
    }
  }

  // if new V > 0 function block
  if(recentChildrenFromVolunteers != null && !recentChildrenFromVolunteers.empty) {
    // getting old P
    let olderPicturesOfParents = await admin.firestore()
      .collectionGroup('childrenAsParent')
      .where('createdAt','<', prevFunctionTime)
      .get();

    // matching
    for(let childDataFromVolunteer of recentChildrenFromVolunteers.docs) {
      for(let childDataFromParent of olderPicturesOfParents.docs) {
        for(let i = 0; i < childDataFromVolunteer.data()['faceImages'].length; i++) {
          for(let j = 0; j < childDataFromParent.data()['faceImages'].length; j++) {
            let matchFound = await matchFaces(
              childDataFromVolunteer.data()['faceImages'][i],
              childDataFromParent.data()['faceImages'][j],
            );
            if(matchFound) {
              await functionOnMatchFound(childDataFromParent,childDataFromVolunteer,i);
              // exiting loop
              i = childDataFromVolunteer.data()['faceImages'].length;
              j = childDataFromParent.data()['faceImages'].length;
            }
          }
        }
      }
    }
  }
});

async function matchFaces(childFromVolunteerFaceImage, childFromParentFaceImage) {
  // matchingConst used for Euclidean distance - range 0 to 1. 0 is 100% match and 1 is 0%
  const matchingConst = 0.6;
  // some genuine matches were left out at 0.5. Tried 0.65, many false matches
  // also, https://justadudewhohacks.github.io/face-api.js/docs/index.html face-api.js package had 0.6 looking reasonable

  try {
    const imgFromVolunteer = await canvas.loadImage(childFromVolunteerFaceImage);
    const imgFromParent = await canvas.loadImage(childFromParentFaceImage);

    const faceImageFromVolunteer = faceapi.createCanvasFromMedia(imgFromVolunteer);
    const faceImageFromParent = faceapi.createCanvasFromMedia(imgFromParent);

    var scannedFaceFromVolunteer = await faceapi.computeFaceDescriptor(faceImageFromVolunteer);
    var scannedFaceFromParent = await faceapi.computeFaceDescriptor(faceImageFromParent);

    const distance = faceapi.euclideanDistance(scannedFaceFromVolunteer, scannedFaceFromParent);

    if (distance <= matchingConst) {
      return true;
    } 
  } catch (e) {
    console.log('error');
    console.log(e);
  }
  return false;
}

async function functionOnMatchFound(childDataFromParent,childDataFromVolunteer,i) {
  console.log('___match found___');
  const parentData = await admin.firestore()
  .collection('users')
  .doc(childDataFromParent.data()['uid'])
  .get();

  if(childDataFromParent.data()['matchedwithChildFromVolunteer'] === undefined) {
    await admin.firestore()
    .collection('users')
    .doc(childDataFromParent.data()['uid'])
    .collection('childrenAsParent')
    .doc(childDataFromParent.data()['id'])
    .update(
      { // adding required info to fetch the child's doc. Add to the app on showMatches button
        'matchedwithChildFromVolunteer': [{
          'volunteerId': childDataFromVolunteer.data()['idOfVolunteerWhoUploaded'],
          'childId': childDataFromVolunteer.data()['id'],
        }]
      }
    );
  } else {
    await admin.firestore()
    .collection('users')
    .doc(childDataFromParent.data()['uid'])
    .collection('childrenAsParent')
    .doc(childDataFromParent.data()['id'])
    .update(
      {
        'matchedwithChildFromVolunteer': admin.firestore.FieldValue.arrayUnion({
          'volunteerId': childDataFromVolunteer.data()['idOfVolunteerWhoUploaded'],
          'childId': childDataFromVolunteer.data()['id'],
        })
      }
    );
  }
  let mapUrl = `https://www.google.com/maps/dir/?api=1&travelmode=driving&layer=traffic&destination=${childDataFromVolunteer.data()['lat']},${childDataFromVolunteer.data()['lng']}`;
  let date=dayjs(childDataFromVolunteer.data()['createdAt'].toDate().toLocaleString()).format('MMM D, YYYY h:mm a');
  sendMail(parentData.data()['email'], parentData.data()['name'], childDataFromParent.data()['name'], childDataFromVolunteer.data()['images'][i], date, mapUrl);
}

function sendMail(email, parentName, childName, fullImageByVolunteer, date, url) {

  const options = {
    'method': 'POST',
    'url': 'https://nanhe-60011477828.development.catalystserverless.in/server/nanhe_function/sendEmail',
    'headers': {
      'Content-Type': 'application/json',
      'Cookie': '3a750b85f1=7c58023b2365c117e2ff9b6feb09efa2; ZD_CSRF_TOKEN=a7e27bbf-83ce-4df9-baa2-83c187dda107; _zcsr_tmp=a7e27bbf-83ce-4df9-baa2-83c187dda107'
    },
    'body': JSON.stringify({
      "toEmail": email,
      "subject": 'Your missing child has been identified!',
      "content": `Dear ${parentName},

We are delighted to inform you that ${childName} has been located by a nanhe volunteer. 

Picture taken by volunteer: ${fullImageByVolunteer}

Location: ${url}

Date & time: ${date} GMT

Please see the picture to confirm that the child shown is indeed ${childName}. If yes, you may take the next steps to pick ${childName}. You might want to seek help of the local police.

Best of luck 👍
    
nanhe`
    }),
  };

  request(options, function (error, response) { 
    if (error) throw new Error(error);
    console.log(response.body);
  });

}